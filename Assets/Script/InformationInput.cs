using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InformationInput : MonoBehaviour
{
    public static event Action<CharacterInfo> OnUpdated;

    [SerializeField] private TMP_InputField _nameField;
    [SerializeField] private TMP_InputField _descriptionField;
    [SerializeField] private TMP_Dropdown _raceDropdown;
    [SerializeField] private TMP_Dropdown _classDropdown;
    [SerializeField] private TextMeshProUGUI _status;


    public void UpdateInfo()
    {
        if (!gameObject.activeInHierarchy)
        {
            return;
        }
        if(_nameField.text.Length == 0)
        {
            OutputStatus("Name field is empty!");
            return;
        }

        if(_descriptionField.text.Length == 0)
        {
            OutputStatus("Description field is empty!");
            return;
        }

        CharacterInfo charInfo = new CharacterInfo();
        charInfo.name = _nameField.text;
        charInfo.description = _descriptionField.text;
        charInfo.race = _raceDropdown.options[_raceDropdown.value].text;
        charInfo.characterClass = _classDropdown.options[_classDropdown.value].text;
        OnUpdated?.Invoke(charInfo);

        OutputStatus("");
        gameObject.SetActive(false);
    }

    private void OutputStatus(string message)
    {
        _status.text = message;
    }
}

public struct CharacterInfo
{
    public string name;
    public string description;
    public string race;
    public string characterClass;
}
