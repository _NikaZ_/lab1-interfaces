using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerInfo : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _nameText;
    [SerializeField] private TextMeshProUGUI _descriptionText;
    [SerializeField] private TextMeshProUGUI _raceText;
    [SerializeField] private TextMeshProUGUI _classText;



    private void OnEnable()
    {
        InformationInput.OnUpdated += UpdateText;
    }

    private void UpdateText(CharacterInfo info)
    {
        _nameText.text = info.name;
        _descriptionText.text = info.description;
        _raceText.text = info.race;
        _classText.text = info.characterClass;
    }

    private void OnDisable()
    {
        InformationInput.OnUpdated -= UpdateText;
    }
}
