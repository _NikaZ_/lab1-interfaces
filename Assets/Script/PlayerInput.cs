using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] private InformationInput _informationInput;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            _informationInput.gameObject.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            _informationInput.UpdateInfo();
        }

    }
}
